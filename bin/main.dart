num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {

    return {'num1' : num2, 'num2': num1};
}

int? countLetter(String letter, String sentence) {
    int count = letter.allMatches(sentence).length;
    if (count == 0){
    return null;
    }else {
        return count;
    }
}

bool isPalindrome(String text) {
    String reverse = '';
    String textSplit = text.split(" ").join("");
    textSplit = textSplit.toUpperCase();

    for(int i=0; i<textSplit.length;i++)
        reverse = reverse + textSplit[(textSplit.length -1) - i];

    if(textSplit == reverse){
        return true;
    }else{
        return false;
    }

}

bool isIsogram(String text) {
    final letters = text.toLowerCase().replaceAll(RegExp(r'\W'), '');
    
    if(letters.length == letters.split('').toSet().length){
        return true;
    }else{
        return false;
    }
}

num? purchase(int age, num price) {

num discountedPrice = price * 0.8;
num roundedPrice = num.parse(discountedPrice.toStringAsFixed(2));

   if (age > 13 && age <= 21){
       return roundedPrice;
   } else if (age >= 22 && age <= 64){
       return num.parse(price.toStringAsFixed(2));
   }else if (age > 64){
       return roundedPrice;
   }else {
       return null;
   }


}

List<String> findHotCategories(List items) {
    
    
    List<String> hotCategories = [];
    for(var item in items){
        if(item["stocks"] == 0){
            if(!hotCategories.contains(item["category"])){
                hotCategories.add(item["category"]);
            }
        }
    }
     return hotCategories;
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    
    List<String> duplicate = [];
    for (int i = 0; i < candidateA.length; i++){
        for(int j =0; j < candidateB.length; j++){
            if(candidateA[i] == candidateB[j]){
                duplicate.add(candidateA[i]);
            }
        }
    }
    return duplicate;
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {

    List<Map<String, dynamic>> brandEfficiency = [];
    items.forEach((item) {
       brandEfficiency.add({
           "brand" : item["brand"],
           "adEfficiency" : item["customersGained"] / item["expenditure"] * 100
       }); 
    });
    brandEfficiency.sort((a, b) => b["adEfficiency"].compareTo(a["adEfficiency"]));
    return brandEfficiency;
}